package com.system

import com.system.oauth.AuthMode
import com.system.oauth.CustomUsernamePasswordToken
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.web.util.SavedRequest
import org.apache.shiro.web.util.WebUtils

class AuthController {

    def index() {
        redirect(action: "login", params: params)
    }

    def login() {
        if (SecurityUtils.getSubject().getPrincipal()) {
            redirect uri: "/"
            return
        }
        render view: 'login', model: [username: params.username, rememberMe: (params.rememberMe != null), targetUri: params.targetUri]
    }

    def noAuth() {
        if (SecurityUtils.getSubject().getPrincipal()) {
            redirect uri: "/"
            return
        }
        render view: 'noAuth', model: [username: params.username, rememberMe: (params.rememberMe != null), targetUri: params.targetUri]
    }

    /**
     *  有密码登录
     * @return
     */
    def signIn() {

        def authToken = new CustomUsernamePasswordToken(params.username, params.password as String, AuthMode.PASSWORD)

        if (params.rememberMe) {
            authToken.rememberMe = true
        }

        def targetUri = params.targetUri ?: "/"

        SavedRequest savedRequest = WebUtils.getSavedRequest(request)
        if (savedRequest) {
            targetUri = savedRequest.requestURI - request.contextPath
            if (savedRequest.queryString) targetUri = targetUri + '?' + savedRequest.queryString
        }

        try {

            SecurityUtils.subject.login(authToken)
            redirect(uri: targetUri)
        } catch (AuthenticationException ex) {
            flash.message = message(code: "login.failed")

            def m = [username: params.username]
            if (params.rememberMe) {
                m["rememberMe"] = true
            }

            if (params.targetUri) {
                m["targetUri"] = params.targetUri
            }

            redirect(action: "login", params: m)
        }
    }

    def signOut() {
        SecurityUtils.subject?.logout()
        redirect(uri: "/")
    }

    def unauthorized() {
        render "<span style='color:red;'>抱歉，你无权访问！<span>"
    }

    /**
     *  无密码登录
     * @return
     */
    def noPass() {

        def authToken = new CustomUsernamePasswordToken(params.username, "", AuthMode.NOPASSWORD)

        if (params.rememberMe) {
            authToken.rememberMe = true
        }

        def targetUri = params.targetUri ?: "/"

        SavedRequest savedRequest = WebUtils.getSavedRequest(request)
        if (savedRequest) {
            targetUri = savedRequest.requestURI - request.contextPath
            if (savedRequest.queryString) targetUri = targetUri + '?' + savedRequest.queryString
        }

        try {

            SecurityUtils.subject.login(authToken)
            redirect(uri: targetUri)
        } catch (AuthenticationException ex) {
            flash.message = message(code: "login.failed")

            def m = [username: params.username]
            if (params.rememberMe) {
                m["rememberMe"] = true
            }

            if (params.targetUri) {
                m["targetUri"] = params.targetUri
            }

            redirect(action: "noAuth", params: m)
        }
    }
}
