package com.system.oauth;

import org.apache.shiro.authc.UsernamePasswordToken;

public class CustomUsernamePasswordToken extends UsernamePasswordToken {

    public CustomUsernamePasswordToken() {
        super();
    }

    // 认证方式（无密码或有密码）
    private AuthMode authMode;

    public AuthMode getAuthMode() {
        return authMode;
    }

    public void setAuthMode(AuthMode authMode) {
        this.authMode = authMode;
    }

    public CustomUsernamePasswordToken(final String username, final String password, AuthMode authMode) {
        super(username, password);
        this.authMode = authMode;
    }

}
