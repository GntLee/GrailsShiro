package com.system.oauth;

public enum AuthMode {

    // 密码登录
    PASSWORD("password"),

    // 无密登录
    NOPASSWORD("nopassword");

    private String code;

    AuthMode(String code) {
        this.code = code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}