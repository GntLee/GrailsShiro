package com.system.oauth;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;

/**
 *  登录校验器
 *  重写HashedCredentialsMatcher类中的doCredentialsMatch方法，实现无密码登录（第三方登录时有用）
 */
public class CustomHashedCredentialsMatcher extends HashedCredentialsMatcher {

    @Override
    public boolean doCredentialsMatch(AuthenticationToken authcToken, AuthenticationInfo info) {
        CustomUsernamePasswordToken token = (CustomUsernamePasswordToken) authcToken;
        // 第三方登录放行
        if (AuthMode.NOPASSWORD.equals(token.getAuthMode())) {
            return true;
        }
        //将密码加密与系统加密后的密码校验，内容一致就返回true,不一致就返回false
        return super.doCredentialsMatch(token, info);
    }

}
