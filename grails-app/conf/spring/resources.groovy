// Place your Spring DSL code here
beans = {

    // 防止hibernate session报错
    openSessionInViewFilter(org.springframework.orm.hibernate4.support.OpenSessionInViewFilter) {}

    // 自定义校验器，主要用于无密登录
    credentialMatcher(com.system.oauth.CustomHashedCredentialsMatcher) {

        // 必须指定加密方式
        hashAlgorithmName = "SHA-256"
    }
}
